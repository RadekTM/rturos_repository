﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using CommonLibrary.Models;
using System.Linq;
using slow_pl_eng.Repository;



namespace slow_pl_eng.Controllers
{
    public class WordController : ApiController
    {
        [HttpGet]
        public IEnumerable<Translation> GetTranslation()
        {
            using (var context = new Context())
            {
                return context.Translations.ToList();

            }
        }

        [HttpGet]
       
        public async Task<Translation> GetAWord(string slowo)
        {
            using (var context = new Context())
            {
                var result = context.Translations.FirstOrDefault(f => f.polishWord == slowo);
                return result;
            }
        }

        [HttpPost]
        public async Task<Translation> NewTranslation(Translation words)
        {
            using (var context = new Context())
            {
                var result = context.Translations.FirstOrDefault(f => f.polishWord == words.polishWord);
                if (result == null)
                {
                    context.Translations.Add(words);
                    await context.SaveChangesAsync();
                }
                else
                {
                    words = result;
                }
            }

            return words;
        }

        [HttpPut]
        public async Task<Translation> UpdateTranslation(Translation newWords)
        {
            using (var context = new Context())
            {
                var result = context.Translations.FirstOrDefault(f => f.polishWord == newWords.polishWord);
                if (result != null)
                {
                    result.englishTranslation = newWords.englishTranslation;
                    await context.SaveChangesAsync();
                }
                else
                {
                    newWords = result;
                }
            }

            return newWords;
        
        }

        [HttpDelete]
        public async Task<Translation> DeleteUser(string slowo)
        {
            using (var context = new Context())
            {
                var translation = context.Translations.FirstOrDefault(f => f.polishWord == slowo);

                context.Translations.Remove(translation);
                await context.SaveChangesAsync();
                return translation;

            }


        }
    }
}