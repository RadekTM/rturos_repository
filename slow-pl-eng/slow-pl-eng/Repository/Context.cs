﻿using System.Data.Entity;
using CommonLibrary.Models;

namespace slow_pl_eng.Repository
{
    public class Context:DbContext
    {
        public DbSet<Translation> Translations { get; set; }

    }
}