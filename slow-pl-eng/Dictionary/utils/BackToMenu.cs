﻿using System;

namespace Dictionary.utils
{
    public class BackToMenu: IBackToMenu
    {
        public void goBackToMenu()
        {
            Console.WriteLine(" ");
            Console.WriteLine("\nAby kontynuować naciśnij dowolny klawisz....");
            Console.ReadKey();
            Console.Clear();
        }
    }
}