﻿using System.Threading.Tasks;

namespace Dictionary
{
    public interface IUserActions
    {
        Task AddTranslation();
        Task GetTranslation();
        Task UpdateTranslation();
        Task DeleteTranslation();
    }
}