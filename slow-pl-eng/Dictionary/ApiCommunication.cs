﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CommonLibrary.Models;
using Newtonsoft.Json;

namespace Dictionary
{
    public class ApiCommunication: IApiCommunication
    {
        public async Task<Translation> AddTranslation(string polishWord, string englishWord)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "word";
                var baseUri = new Uri(ConfigurationManager.AppSettings["Endpoint"]);
                var uri = new Uri(baseUri, adress);
                var words = new Translation
                {
                    polishWord = polishWord,
                    englishTranslation = englishWord
                };

                var userSerialized = JsonConvert.SerializeObject(words);

                var getResponce = await httpClient.PostAsync(uri, new StringContent(userSerialized, Encoding.UTF8, "application/json"));
                var userString = await getResponce.Content.ReadAsStringAsync();

                var translation = JsonConvert.DeserializeObject<Translation>(userString);

                return translation;
            }

        }

        public async Task<Translation> GetTranslation(string polishWord)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                    var adress = "word?slowo=" + polishWord;
                    var baseUri = new Uri(ConfigurationManager.AppSettings["Endpoint"]);
                    var uri = new Uri(baseUri, adress);

                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var getResponce = await httpClient.GetAsync(uri);

                    getResponce.EnsureSuccessStatusCode();

                    var userString = await getResponce.Content.ReadAsStringAsync();

                    var translation = JsonConvert.DeserializeObject<Translation>(userString);

                    return translation;
                
            }

        }

        public async Task<Translation> UpdateTranslation(string polishWord, string englishWord)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "word";
                var baseUri = new Uri(ConfigurationManager.AppSettings["Endpoint"]);
                var uri = new Uri(baseUri, adress);
                var words = new Translation
                {
                    polishWord = polishWord,
                    englishTranslation = englishWord
                };

                var userSerialized = JsonConvert.SerializeObject(words);

                var getResponce = await httpClient.PutAsync(uri,
                    new StringContent(userSerialized, Encoding.UTF8, "application/json"));
                var userString = await getResponce.Content.ReadAsStringAsync();

                var translation = JsonConvert.DeserializeObject<Translation>(userString);

                return translation;
            }

        }

        public async Task<Translation> DeleteTranslation(string polishWord)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "word?slowo=" + polishWord;
                var baseUri = new Uri(ConfigurationManager.AppSettings["Endpoint"]);
                var uri = new Uri(baseUri, adress);

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var getResponce = await httpClient.DeleteAsync(uri);

                getResponce.EnsureSuccessStatusCode();

                var userString = await getResponce.Content.ReadAsStringAsync();

                var removed = JsonConvert.DeserializeObject<Translation>(userString);

                return removed;
            }
        }


    }
}