﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Dictionary
{
    public class UserActions:IUserActions
    {
        private readonly IApiCommunication apiCommunication = new ApiCommunication();

        public async Task AddTranslation()
        {
            try
            {
                Console.Write("Podaj polskie słowo: ");
                string polishWord = Console.ReadLine();
                Console.Write("\nPodaj angielskie tłumaczenie: ");
                string englishTranslation = Console.ReadLine();
                var result = await apiCommunication.AddTranslation(polishWord, englishTranslation);
                if (result.id == 0)
                {
                    Console.WriteLine("Wpis został dodany do bazy");
                }
                else
                {
                    Console.WriteLine("Taki wpis już istnieje w bazie");
                }

            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task GetTranslation()
        {
            try
            {
                Console.Write("Podaj polskie słowo:");
                string polishWord = Console.ReadLine();
                var result = await apiCommunication.GetTranslation(polishWord);
                if (result != null)
                {
                    Console.WriteLine(result);
                }
                else
                {
                    Console.WriteLine("Brak tłumaczenia w bazie");
                }

            }
            catch (HttpRequestException e )
            {
                Console.WriteLine(e);
            }
        }

        public async Task UpdateTranslation()
        {
            try
            {
                Console.Write("Podaj polskie słowo: ");
                string polishWord = Console.ReadLine();
                Console.Write("\nPodaj uaktualnione/nowe angielskie tłumaczenie: ");
                string englishTranslation = Console.ReadLine();
                var result = await apiCommunication.UpdateTranslation(polishWord, englishTranslation);
                if (result.id == 0)
                {
                    Console.WriteLine("Wpis został uaktualniony bazy");
                }
                else
                {
                    Console.WriteLine("Taki wpis już istnieje w bazie");
                }

            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task DeleteTranslation()
        {
            try
            {
                Console.Write("Podaj polskie słowo  (do usunięcia) :");
                string polishWord = Console.ReadLine();
                var result = await apiCommunication.DeleteTranslation(polishWord);
                if (result != null)
                {
                    Console.WriteLine($"Tłumaczenie słowa {result.polishWord} zostało usunięte");
                }
                else
                {
                    Console.WriteLine("Brak tłumaczenia w bazie");
                }

            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e);
            }
        }
    }
}