﻿using System.Threading.Tasks;
using CommonLibrary.Models;

namespace Dictionary
{
    public interface IApiCommunication
    {
        Task<Translation> AddTranslation(string polishWord, string englishWord);
        Task<Translation> GetTranslation(string polishWord);
        Task<Translation> UpdateTranslation(string polishWord, string englishWord);
        Task<Translation> DeleteTranslation(string polishWord);
    }
}