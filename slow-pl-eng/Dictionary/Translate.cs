﻿using System;
using System.Threading.Tasks;
using Dictionary.utils;

namespace Dictionary
{
    public class Translate
    {
        private readonly IUserActions userActions = new UserActions();
        private readonly IBackToMenu goBack = new BackToMenu();
        public async Task start()
        {
            bool menu = true;
            do
            {
                Console.WriteLine("a - Dodaj tłumaczenie");
                Console.WriteLine("b - znajdź tłumaczenie");
                Console.WriteLine("c - uaktualnij tłumaczenie");
                Console.WriteLine("d - usuń tłumaczenie");
                Console.WriteLine("x - wyjście");
                char menuitem = Console.ReadKey().KeyChar;
                Console.Clear();

                switch (menuitem)
                {
                    case 'a':
                        await userActions.AddTranslation();
                        goBack.goBackToMenu();
                        break;
                    case 'b':
                        await userActions.GetTranslation();
                        goBack.goBackToMenu();
                        break;
                    case 'c':
                        await userActions.UpdateTranslation();
                        goBack.goBackToMenu();
                        break;
                    case 'd':
                        await userActions.DeleteTranslation();
                        goBack.goBackToMenu();
                        break;
                    case 'x':
                        menu = false;
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Opcja niedostęna, proszę wybrać ponownie");
                        Console.Clear();
                        break;

                }
            } while (menu == true);
        }
    }
}
