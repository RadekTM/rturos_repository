﻿namespace CommonLibrary.Models
{
    public class Translation
    {
        public int id { get; set; }
        public string polishWord { get; set; }
        public string englishTranslation { get; set; }

        public override string ToString()
        {
            return $"Słowo {polishWord} - Tłumaczenie na j.angielski: {englishTranslation}";
        }
    }
}