﻿namespace Infrastructure
{
    public interface IUserValues
    {
        int ValueX();
        int ValueY();
        double PowValueX();
        double PowValueY();
    }
}
