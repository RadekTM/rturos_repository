﻿namespace Infrastructure
{
    public interface IResultRecorder
    {
        void OperationFinished(object sender, MyEventArgs e);
    }
}
