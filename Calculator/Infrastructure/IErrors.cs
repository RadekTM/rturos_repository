﻿using Infrastructure;

namespace ErrorHandler
{
    public interface IErrors
    {
        int ErrorFormatMessage();
        double ErrorDoubleFormatMessage();
        void OperationError(object sender, ErrorEventArgs e);
    }
}