﻿namespace Infrastructure
{
    public interface IUserImputData
    {
        int ImputValueX();
        int ImputValueY();
        double ImputPowValueX();
        double ImputPowValueY();
    }
}
