﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Communication
{
    public class UserInfos : IUserInfos
    {
        public void EquationResult(int result)
        {
            Console.WriteLine($"wynikiem działania jest:{result}");
        }

        public void OpperationName(string name)
        {
            Console.WriteLine($"Kalkulator: {name}");
        }
    }
}
