﻿using Interfaces.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Communication
{
    public class UserValues : IUserValues
    {
        public int ValueX()
        {
            return int.Parse(Console.ReadLine());
        }

        public int ValueY()
        {
            return int.Parse(Console.ReadLine());
        }

        public double PowValueX()
        {
            return double.Parse(Console.ReadLine());
        }

        public double PowValueY()
        {
            return double.Parse(Console.ReadLine());
        }

    }
}
