﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HistoryLogger
{
    public class Module : NinjectModule
    {
        public override void Load()
        {
            Bind<IResultRecorder>().To<ResultRecorder>();
        }
    }
}
