﻿using Infrastructure;
using System;
using System.IO;

namespace ErrorHandler
{
    public class Errors : IErrors
    { 
    
        private readonly ICalculator _calc;
        public Errors(ICalculator calc)
        {
            _calc = calc;
            _calc.OperationError += OperationError;

        }

        public void OperationError(object sender, Infrastructure.ErrorEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Działanie zakończono z błedem");
            Console.ResetColor();

            using (var sw = new StreamWriter(File.Open("Errorlog.txt", FileMode.Append,
                FileAccess.Write)))
            {
                sw.WriteLine($">{DateTime.Now}< wykonano działanie: {e.OpperationName}, Działanie zakończono z błedem");
            }
            _calc.OperationError -= OperationError;
        }
   


        public int ErrorFormatMessage()
        {
            try
            {

                return int.Parse(Console.ReadLine());

            }
            catch (FormatException)
            {

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Nieprawidlowy format ciagu wejsciowego.");
                Console.WriteLine("Wartość parametru = 0");
                Console.ResetColor();

                return 0;
            }
        }

        public double ErrorDoubleFormatMessage()
        {
            try
            {

                return double.Parse(Console.ReadLine());

            }
            catch (FormatException)
            {

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Nieprawidlowy format ciagu wejsciowego.");
                Console.WriteLine("Wartość parametru = 0");
                Console.ResetColor();

                return 0;
            }
        }

    }
}
