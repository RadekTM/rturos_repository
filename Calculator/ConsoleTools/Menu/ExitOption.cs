﻿using ConsoleTools.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTools.Menu
{
    public class ExitOption : IMenuOption
    {
        public string Description { get; } = "Exit";
        public bool IsExitRequested { get; private set; }

        public void Execute()
        {
            IsExitRequested = true;
        }
    }
}
