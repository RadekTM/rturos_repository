﻿using Infrastructure;
using System;

namespace ConsoleApp11
{

    public class Calculator : ICalculator
    {
        public event EventHandler<MyEventArgs> OperationFinished;
        public event EventHandler<ErrorEventArgs> OperationError;

        public int SummTwoNumbers(int a, int b, string name)
        {
            try
            {
                int x = a + b;

                OperationFinished?.Invoke(this, new MyEventArgs {Result = x, OpperationName = name});
                return x;
            }
            catch (FormatException)
            {
                OperationError?.Invoke(this, new ErrorEventArgs { OpperationName = name });
                return 0;
            }
        }

        public int DiferenceTwoNumbers(int a, int b, string name)
        {
            int x = a - b;

            OperationFinished?.Invoke(this, new MyEventArgs { Result = x, OpperationName = name });
            return x;
        }

        public int MultiplyOfTwoNumbers(int a, int b, string name)
        {
            int x = a * b;

            OperationFinished?.Invoke(this, new MyEventArgs { Result = x, OpperationName = name });
            return x;
        }

        public int DivideTwoNumbers(int a, int b, string name)
        {

            try
            {
                int x = a / b;
                OperationFinished?.Invoke(this, new MyEventArgs { Result = x, OpperationName = name });
                return x;
            }
            catch (DivideByZeroException)
            {
                OperationError?.Invoke(this, new ErrorEventArgs { OpperationName = name});
                return 0;
            }
            
        }

        public int PowerOfTwoNumbers(double a, double b, string name)
        {
            int x = (int)Math.Pow(a, b);

            OperationFinished?.Invoke(this, new MyEventArgs { Result = x, OpperationName = name });
            return x;
        }

        public int OperationXOR(int a, int b, string name)
        {
            int x = a ^ b;
            OperationFinished?.Invoke(this, new MyEventArgs { Result = x, OpperationName = name });
            return x;
        }

        public int OperationOR(int a, int b, string name)
        {
            int x = a | b;
            OperationFinished?.Invoke(this, new MyEventArgs { Result = x, OpperationName = name });
            return x;
        }

        public int OperationAND(int a, int b, string name)
        {
            int x = a | b;
            OperationFinished?.Invoke(this, new MyEventArgs { Result = x, OpperationName = name });
            return x;
        }
    }
}
