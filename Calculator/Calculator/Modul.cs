﻿using Calculator.UserCommunication;
using ErrorHandler;
using HistoryLogger;
using Infrastructure;
using Ninject.Modules;

namespace ConsoleApp11
{
    public class Modul : NinjectModule
    {
        public override void Load()
        {
            Bind<ICalculator>().To<Calculator>().InSingletonScope();
            Bind<IResultRecorder>().To<ResultRecorder>();
            Bind<IUserImputData>().To<UserImputData>();
            Bind<IUserInfos>().To<UserInfos>();
            Bind<IUserValues>().To<UserValues>();
            Bind<IErrors>().To<Errors>();
        }
    }
}
