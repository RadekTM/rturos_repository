﻿using Infrastructure;
using System;

namespace Calculator.UserCommunication
{
    public class UserInfos : IUserInfos
    {
        public void EquationResult(int result)
        {
            Console.WriteLine($"wynikiem działania jest:{result}");
        }

        public void OpperationName(string name)
        {
            Console.WriteLine($"Kalkulator: {name}");

        }
    }
}
