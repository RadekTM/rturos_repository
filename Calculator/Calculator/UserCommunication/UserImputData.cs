﻿using Infrastructure;
using System;

namespace Calculator.UserCommunication
{
    public class UserImputData : IUserImputData
    {
        private IUserValues _userValues;

        public UserImputData(IUserValues userValues)
        {
            _userValues = userValues;
        }

        public int ImputValueX()
        {
            Console.WriteLine("Podaj wartość x");
            return _userValues.ValueX();
        }

        public int ImputValueY()
        {
            Console.WriteLine("Podaj wartość y");
            return _userValues.ValueY();
        }

        public double ImputPowValueX()
        {
            Console.WriteLine("Podaj wartość podstawy");
            return _userValues.PowValueX();
        }

        public double ImputPowValueY()
        {
            Console.WriteLine("Podaj wartość potęgi");
            return _userValues.PowValueY();
        }

    }
}
