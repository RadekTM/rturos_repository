﻿using System;

namespace Calculator.UserCommunication
{
    public class ConsoleOps
    {
        public void EndOfCalculations()
        {
            Console.WriteLine("Naciśniej dowolny klawisz aby powrócić do głównego menu...");
            Console.ReadKey();
            Console.Clear();
        }
        
    }
}
