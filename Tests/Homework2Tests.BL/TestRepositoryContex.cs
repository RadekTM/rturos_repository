﻿using Homework2Tests.BL.Model;
using System.Data.Entity;

namespace Homework2Tests.BL
{

    public class TestRepositoryContex : DbContext
    {

        public TestRepositoryContex() : base("name=TestRepository")
        {

        }

        public DbSet<Cathegory> Cathegory { get; set; }
        public DbSet<TestQuestion> TestQuestion { get; set; }
        public DbSet<Answers> Answers { get; set; }
    }
   
}
