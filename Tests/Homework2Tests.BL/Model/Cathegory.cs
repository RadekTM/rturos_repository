﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework2Tests.BL.Model
{
    public class Cathegory
    {
        public int ID { get; set; }
        public string CathegoryName { get; set; }

        public virtual ICollection<TestQuestion> Question { get; set; }
                = new List<TestQuestion>();

    }
}
