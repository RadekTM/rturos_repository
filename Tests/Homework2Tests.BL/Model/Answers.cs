﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework2Tests.BL.Model
{
    public class Answers
    {
        public int ID { get; set; }
        public string Answer { get; set; }
        public bool CorrectAnswer { get; set; }
        public virtual TestQuestion Quest { get; set; }

    }
}
