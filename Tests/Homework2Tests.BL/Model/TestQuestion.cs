﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework2Tests.BL.Model
{
    public class TestQuestion
    {
        public int ID { get; set; }
        public string Question { get; set; }
        public virtual IList<Answers> AnswersList {get;set;}
       
        public Cathegory Cathegory { get; set; }

    }
}
