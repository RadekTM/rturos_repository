﻿using Homework2Tests.BL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Newtonsoft.Json;

namespace Homework2Tests.BL
{
    public class TestRepository
    {
        CathegoryChecker cathegoryChecker = new CathegoryChecker();
        StringChecker stringChecker = new StringChecker();
        GoBack GoBack = new GoBack();

        public void AddCathegory()
        {
            Console.Clear();
            var _cathegory = new Cathegory();
            string _cathegoryName = stringChecker.NullString("Podaj nazwę nowej kategorii: ");
            
            using (var context = new TestRepositoryContex())
            {

                if (cathegoryChecker.CathegoryCheck(_cathegoryName) == null)
                {
                    _cathegory.CathegoryName = _cathegoryName;

                    context.Cathegory.Add(_cathegory);

                    context.SaveChanges();
                    Console.WriteLine("Kategoria została dodana");
                }
                else
                {
                    Console.WriteLine("Kategoria istnieje już w bazie");
                }
                GoBack.ClearAndGoBack();
            }
        }

        public void CathegoryList()
        {
            Console.Clear();
            List<Cathegory> cathegories;
            using (var context = new TestRepositoryContex())
            {
                cathegories = context.Cathegory.ToList();
            }
            foreach (var item in cathegories)
            {
                Console.WriteLine(item.CathegoryName);
            }
            GoBack.ClearAndGoBack();
        }

        public void AddTestQuestion()
        {
            Console.Clear();
            string _cathegoryName = stringChecker.NullString("Podaj nazwę kategorii: ");
            Cathegory cathegory;

            using (var context = new TestRepositoryContex())
            {
                var testQuestion = new TestQuestion();
                cathegory = context.Cathegory.FirstOrDefault
                    (a => a.CathegoryName.Equals(_cathegoryName, StringComparison.InvariantCultureIgnoreCase)
                );
                
                if (cathegory != null)
                {
                    testQuestion.Question = stringChecker.NullString("Podaj pytanie: ");
                    testQuestion.Cathegory = cathegory; 
                    testQuestion = context.TestQuestion.Add(testQuestion);
                    context.SaveChanges();

                    for (int i = 1; i <= 4; i++)
                    {
                        var answer = new Answers();
                        if (i == 1)
                        {
                            string myAnswer = stringChecker.NullString("\nPodaj pierwszą (poprawną) odpowiedź: ");
                            answer.Answer = myAnswer;
                            answer.CorrectAnswer = true;
                        }
                        else
                        {
                            string myAnswer = stringChecker.NullString($"\nPodaj odpowiedź nr.{i} (błędą odpowiedź): ");
                            answer.Answer = myAnswer;
                            answer.CorrectAnswer = false;
                        }

                        answer.Quest = testQuestion;
                        context.Answers.Add(answer);
                        
                    }
                    
                    context.SaveChanges();
                    Console.WriteLine("Pytanie zostało dodane do bazy.");
                }
                else
                {
                  Console.WriteLine("Kategoria nie istnieje w bazie!");
                }

            }
            GoBack.ClearAndGoBack();
        }

        public void Test()
        {
            Console.Clear();
            string _cathegoryName = stringChecker.NullString("Podaj nazwę kategorii: ");
            Result result = new Result();
            Cathegory cathegory;
            int corrAnsw = 0, TotalQuestions = 0;

            using (var context = new TestRepositoryContex())
            {

                if (cathegoryChecker.CathegoryCheck(_cathegoryName) == null)
                {
                    Console.WriteLine("Kategoria nie istnieje w bazie");
                }
                else
                {
                    Console.Clear();
                    
                    cathegory = context.Cathegory.FirstOrDefault
                    (a => a.CathegoryName.Equals(_cathegoryName, StringComparison.InvariantCultureIgnoreCase));
                    if (cathegory.Question.Count  != 0)
                    {
                        Console.WriteLine($"Wybrana kategoria to {_cathegoryName}");
                        foreach (var question in RandomPermutation(cathegory.Question))
                        {

                            Console.WriteLine(question.Question);

                            foreach (var answer in RandomPermutation(question.AnswersList))
                            {
                                Console.WriteLine(answer.Answer);
                            }

                            var userAnswer = stringChecker.NullString("Podaj swoją odpowiedz:");
                            Answers tempAnswer;

                            tempAnswer = question.AnswersList.FirstOrDefault(a => a.Answer
                            .Equals(userAnswer, StringComparison.CurrentCultureIgnoreCase)
                            && a.CorrectAnswer);

                            if (tempAnswer != null)
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("Odpowiedź poprawna");
                                corrAnsw++;
                                Console.ResetColor();
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Błędna odpowiedź");
                                Console.ResetColor();
                            }
                        }

                        TotalQuestions = cathegory.Question.Count();
                        result.CorrectAnswers = corrAnsw;
                        result.AllQuestions = TotalQuestions;
                        result.Time = DateTime.Now;
                        result.CathegoryName = cathegory.CathegoryName;
                        Console.WriteLine(result);
                        var currentDirectory = Environment.CurrentDirectory;
                        var pathToDir = Path.Combine(currentDirectory, "Results");
                        var pathToFile = Path.Combine(pathToDir, "LatestScore.json");

                        Serialize(result, pathToFile);
                    }
                    else
                    {
                        Console.WriteLine($"Brak pytań w kategorii: {_cathegoryName}");
                    }
                    
                }
            }
            GoBack.ClearAndGoBack();
        }

        public static IEnumerable<T> RandomPermutation<T>(IEnumerable<T> sequence)
        {
            T[] retArray = sequence.ToArray();
            Random random = new Random();

            for (int i = 0; i < retArray.Length - 1; i += 1)
            {
                int swapIndex = random.Next(i, retArray.Length);
                if (swapIndex != i)
                {
                    T temp = retArray[i];
                    retArray[i] = retArray[swapIndex];
                    retArray[swapIndex] = temp;
                }
            }

            return retArray;
        }

        public static void Serialize(Result _result, string PathToFile)
        {
            List<Result> results;
            try
            {
                string filelocalization = File.ReadAllText(PathToFile);
                results = JsonConvert.DeserializeObject<List<Result>>(filelocalization);
            }
            catch
            {
                results = new List<Result>();

            }
            results.Add(_result);

            string jsonFile = JsonConvert.SerializeObject(results);

            File.WriteAllText(PathToFile, jsonFile);
        }
    }
}
