﻿using Homework2Tests.BL.Model;
using System;
using System.Linq;

namespace Homework2Tests.BL
{
    public class CathegoryChecker
    {
        public Cathegory CathegoryCheck (string _cathegoryName)
        {
            Cathegory cathegory;
            using (var context = new TestRepositoryContex())
            cathegory = context.Cathegory.
                FirstOrDefault
                (a => a.CathegoryName.Equals(_cathegoryName, StringComparison.InvariantCultureIgnoreCase));
            return cathegory;
        }
    }
}
