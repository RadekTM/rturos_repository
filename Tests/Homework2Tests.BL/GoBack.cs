﻿using System;

namespace Homework2Tests.BL
{
    internal class GoBack
    {
        public void ClearAndGoBack()
        {
            Console.WriteLine("Naciśnij dowolny klawisz aby powrócić do menu..");
            Console.ReadKey();
            Console.Clear();
        }
    }
}
