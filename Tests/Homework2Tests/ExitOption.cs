﻿using ConsoleTools.Menu;

namespace Homework2Tests
{
    internal class ExitOption : IMenuOption
    {
        public string Description { get; } = "Exit";
        public bool IsExitRequested { get; private set; }

        public void Execute()
        {
            IsExitRequested = true;
        }
    }
}
