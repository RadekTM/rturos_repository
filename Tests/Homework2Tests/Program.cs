﻿using System;
using ConsoleTools.Menu;
using Homework2Tests.BL;

namespace Homework2Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            TestRepository testRepository = new TestRepository();
            var exitOption = new ExitOption();
            
            var consoleMenu = new ConsoleMenu(new IMenuOption[] {
                new GeneralOption("Dodanie nowej kategorii",testRepository.AddCathegory),
                new GeneralOption("Spis kategorii",testRepository.CathegoryList),
                new GeneralOption("Dodaj pytanie do bazy testów",testRepository.AddTestQuestion),
                new GeneralOption("Przetestuj swoją wiedzę",testRepository.Test),
                exitOption
            });

            do
            {
                consoleMenu.Show();
            } while (!exitOption.IsExitRequested);
            Console.ReadLine();
        }

    }
}
