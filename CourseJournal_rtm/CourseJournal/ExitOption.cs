﻿using ConsoleTools.Menu;

namespace CourseJournal
{
    internal class ExitOption : IMenuOption
    {
        public string Description { get; } = "Exit";
        public bool IsExitRequested { get; private set; }

        public void Execute()
        {
            IsExitRequested = true;
        }
    }
}
