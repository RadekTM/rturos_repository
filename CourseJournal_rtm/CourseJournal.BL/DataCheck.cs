﻿using System;

namespace CourseJournal.BL
{
    public class DataCheck
    {
        public DateTime DataChecker()
        {
            DateTime dateValue;
            bool CorrectYear = true;
            do
            {
                Console.Write($"Proszę podać date w formacie (DD-MM-YYYY):");
                var TrueDate = DateTime.TryParse(Console.ReadLine(), out dateValue);
                if (TrueDate)
                {
                    CorrectYear = true;
                }
                else
                {
                    CorrectYear = false;
                }

            } while (CorrectYear == false);
            return dateValue;

        }
    }
}
