﻿namespace CourseJournal.BL.Model
{
    public class Person
    {
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
