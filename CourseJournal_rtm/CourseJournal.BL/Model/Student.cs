﻿using System;

namespace CourseJournal.BL.Model
{
    public class Student
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public uint Id { get; set; }
        public DateTime BirthDate { get; set; }
        public char Gender { get; set; }

        public Student(string SName, string SSurname, uint Sid, DateTime SBirthdate, char Sgender)

        {
            Name = SName;
            Surname = SSurname;
            Id = Sid;
            BirthDate = SBirthdate;
            Gender = Sgender;
        }

    }
}

