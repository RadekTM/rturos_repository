﻿namespace CourseJournal.BL.Model
{
    public class Teacher
    {
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
