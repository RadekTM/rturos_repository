﻿namespace Homework1
{
    internal class Books
    {
        public string AuthorName { get; set; }
        public string AuthorSureName { get; set; }
        public string BookTitle { get; set; }
        public uint PublicationYear { get; set; }
        public string BookCollectionTitle { get; set; }
        public bool InCollection { get; set; }



        public Books(string tempName, string tempSureName, string tempTitle, uint tempYear, string tempCollectionTitle, bool tempInCollection)
        {
            AuthorName = tempName;
            AuthorSureName = tempSureName;
            BookTitle = tempTitle;
            PublicationYear = tempYear;
            BookCollectionTitle = tempCollectionTitle;
            InCollection = tempInCollection;
        }
    }
}

