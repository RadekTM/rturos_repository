﻿using System;

namespace Homework1
{
    class ConsoleMenu
    {
        public void ManiMenu()
        {
            Library collection = new Library();
            BackToMenu goback = new BackToMenu();
            bool menu = true;
            var book = new Library();

            do
            {
                Console.WriteLine("Tworzenie Zbioru Książek");
                Console.WriteLine("\na - Dodawanie nowej książki do kolekcji");
                Console.WriteLine("b - Wyszykiwanie książki po tytule");
                Console.WriteLine("c - Wyszukiwanie Książki po nazwisku Autora");
                Console.WriteLine("e - Wyjście z programu");
                char menuitem = Console.ReadKey().KeyChar;

                switch (menuitem)
                {
                    case 'a':
                        AddingBookToStore();
                        goback.goBackToMenu();
                        break;
                    case 'b':
                        collection.SearchingByTitle();
                        goback.goBackToMenu();
                        break;
                    case 'c':
                        collection.SearchingByAuthor();
                        goback.goBackToMenu();
                        break;
                    case 'e':
                        menu = false;
                        Console.ResetColor();
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Opcja niedostęna, proszę wybrać ponownie");
                        Console.Clear();
                        break;
                }
            }
            while (menu == true);

            void AddingBookToStore()
            {
                Console.Clear();
                Console.WriteLine("Dodawanie nowej pozycji do zbioru księgarni");
                bool creation = true;
                do
                {
                    Console.Write("\nProszę podać imię Autora książki:  ");
                    string BName = Convert.ToString(Console.ReadLine());
                    do
                    {
                        if (string.IsNullOrEmpty(BName))
                        {
                            Console.Write($"Nie podano imienia autora - proszę podać imię Autora książki:  ");
                            BName = Convert.ToString(Console.ReadLine());
                        }
                    } while (string.IsNullOrEmpty(BName) == true);

                    Console.Write($"\nProszę Nazwisko Autora książki: ");
                    string BSurname = Console.ReadLine();
                    do
                    {
                        if (string.IsNullOrEmpty(BSurname))
                        {
                            Console.Write($"Nie podano nazwiska autora - proszę podać nazwisko Autora książki:  ");
                            BSurname = Convert.ToString(Console.ReadLine());
                        }
                    } while (string.IsNullOrEmpty(BSurname) == true);

                    Console.Write($"\nProszę podać Tytuł książki: ");
                    string BTitle = Console.ReadLine();
                    do
                    {
                        if (string.IsNullOrEmpty(BTitle))
                        {
                            Console.Write($"Nie podano Tutułu książki - proszę podać Tytuł książki:  ");
                            BTitle = Convert.ToString(Console.ReadLine());
                        }
                    } while (string.IsNullOrEmpty(BTitle) == true);

                    //zmienne pomocnicze
                    uint BDate = 0;
                    bool CorrectYear = false;
                    Console.WriteLine("");
                    do
                    {
                        Console.Write($"Proszę podać rok publikacji książki (w formacie YYYY): ");
                        var TrueDate = uint.TryParse(Console.ReadLine(), out uint year);
                        if (TrueDate)
                        {
                            BDate = year;
                            CorrectYear = true;
                        }
                        if (BDate.ToString().Length > 4)
                        {
                            CorrectYear = false;
                        }
                        else if (BDate > DateTime.Today.Year)
                        {
                            CorrectYear = false;
                        }
                        else if (BDate < 1800)
                        {
                            CorrectYear = false;
                        }
                    } while (CorrectYear == false);

                    Console.Write("\nCzy książka należy do kolekcji lub serii (t/n): ");
                    char Collection = Console.ReadKey().KeyChar;
                    string BcollectionName;
                    bool BInCollection = false;
                    if (Collection == 'T' || Collection == 't')
                    {
                        BInCollection = true;
                        Console.Write("\n\nProszę podać tytuł kolekcji/serii: ");
                        BcollectionName = Console.ReadLine();
                    }
                    else
                    {
                        BcollectionName = " ";
                    }
                    collection.AddBook(new Books(BName, BSurname, BTitle, BDate, BcollectionName, BInCollection));

                    Console.Write("\nCzy dodać kolejną książkę (t/n): ");
                    char newBook = Console.ReadKey().KeyChar;
                    if (newBook == 'T' || newBook == 't')
                    {
                        Console.WriteLine(" ");
                        creation = true;
                    }
                    else
                    {
                        creation = false;
                    }
                } while (creation == true);
            }
        }
    }
}

