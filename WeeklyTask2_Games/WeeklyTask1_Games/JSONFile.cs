﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeeklyTask1_Games.DataAccess.Entites;

namespace WeeklyTask1_Games
{
    public class JSONFile:IJSONFile
    {
        public class Result
        {
            public string PlayerName { get; set; }
            public int Score { get; set; }
        }

        public void Serialize(IList<GameScore> gameScores)
        {
            List<Result> results = new List<Result>();
            foreach (var res in gameScores)
            {
                var result = new Result()
                {
                    PlayerName = res.PlayerName,
                    Score = res.Score
                };
                results.Add(result);
            }

            string jsonFile = JsonConvert.SerializeObject(results);

            string filename = "results.json";
            File.WriteAllText(filename, jsonFile);
        }
    }
}
