﻿using Ninject;

namespace WeeklyTask1_Games
{
    internal static class Program
    {
        private static void Main()
        {

            IKernel kernel = new StandardKernel(new Module());

            var gameExecutor = kernel.Get<IGameExecutor>();
            var GuessGame = kernel.Get<IGame>();
            var RecordResults = kernel.Get<IResultRecorder>();

            while (true)
            {
                gameExecutor.GamePlay();
            }
        }
    }
}
