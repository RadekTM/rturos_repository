﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeeklyTask1_Games.DataAccess;
using WeeklyTask1_Games.DataAccess.Entites;

namespace WeeklyTask1_Games
{
    public class Module:NinjectModule
    {
        public override void Load()
        {
            Bind<IGameExecutor>().To<GameExecutor>().InSingletonScope();
            Bind<IGame>().To<GuessGame>();
            Bind<IResultRecorder>().To<ResultRecorder>();
            Bind<IScoreRepository>().To<ScoresRepository>();
            Bind<IJSONFile>().To<JSONFile>();
        }
    }
}
