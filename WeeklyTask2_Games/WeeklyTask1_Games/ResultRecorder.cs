﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Runtime.InteropServices;
using static WeeklyTask1_Games.JSONFile;

namespace WeeklyTask1_Games
{
    public class ResultRecorder : IResultRecorder
    {
        private readonly IGameExecutor _game;
        private readonly IJSONFile _jSONFile;
 

        public ResultRecorder(IGameExecutor game, IJSONFile jSONFile)
        {
            _game = game;
            _jSONFile = jSONFile;
            _game.GameFinished += GameFinished;
        }

        
        public void GameFinished(object sender, MyEventArgs e)
        {
            Console.WriteLine($"{e.Imput}");
            Console.WriteLine("Twoje poprzednie wyniki to:");
            foreach (var score in e.ListOfResults)
            {
                
                Console.WriteLine(score.Score);
            }
            Console.WriteLine("Czy chesz zapisać swoje wyniki do pliku (t/n)");
            char answer = Console.ReadKey().KeyChar;
            if (answer == 't')
            {
                _jSONFile.Serialize(e.ListOfResults);
            }
            Console.Clear();
        }


    }

}
