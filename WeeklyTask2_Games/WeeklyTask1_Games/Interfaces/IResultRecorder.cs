﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeeklyTask1_Games
{
    public interface IResultRecorder
    {
       void GameFinished(object sender, MyEventArgs e);
    }
}
