﻿using System.Collections.Generic;
using WeeklyTask1_Games.DataAccess.Entites;

namespace WeeklyTask1_Games
{
    public interface IJSONFile
    {
        //void Serialize(Result _result);
        void Serialize(IList<GameScore> gameScores);
    }
}