﻿using System;
using System.Collections.Generic;
using WeeklyTask1_Games.DataAccess.Entites;

namespace WeeklyTask1_Games
{

    public class MyEventArgs : EventArgs
    {
        public string Imput { get; }
        public readonly IList<GameScore> ListOfResults;

        public MyEventArgs(IList<GameScore> ListOfResults, string imput)
        {
            this.ListOfResults = ListOfResults;
            Imput = imput;
        }

    }

    public interface IGameExecutor
    {
       void GamePlay();
       event EventHandler<MyEventArgs> GameFinished;
    }
}