﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeeklyTask1_Games.DataAccess.Entites;

namespace WeeklyTask1_Games
{
    public interface IScoreRepository
    {
        void Add(GameScore score);
        IList<GameScore> GetAll();
    }
}
