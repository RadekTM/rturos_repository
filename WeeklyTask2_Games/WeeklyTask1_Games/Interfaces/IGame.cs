﻿using System;

namespace WeeklyTask1_Games
{
    public interface IGame
    {
        string Name { get; }
        int Score { get; }

        void Play();
        void ResetScore();
    }
}
