﻿using System.Configuration;
using System.Data.Entity;
using WeeklyTask1_Games.DataAccess.Entites;

namespace WeeklyTask1_Games.DataAccess
{
    internal class ScoresDbContext : DbContext
    {
        public ScoresDbContext() : base(GetConnectionString())
        {
        }

        public DbSet<GameScore> GameScores { get; set; }

        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["ScoresDb"].ConnectionString;
        }
    }
}