﻿using System;
using System.Collections.Generic;
using WeeklyTask1_Games.DataAccess;
using WeeklyTask1_Games.DataAccess.Entites;

namespace WeeklyTask1_Games
{
    public class GameExecutor:IGameExecutor
    {
        private readonly IGame _game;
        private readonly IScoreRepository _scoresRepository;
        public event EventHandler<MyEventArgs> GameFinished;

        public GameExecutor(IGame game, IScoreRepository scoresRepository)
        {
            _game = game;
            _scoresRepository = scoresRepository;
        }

        public void GamePlay()
        {
            Console.WriteLine("You play " + _game.Name + " game!");

            var gameScore = new GameScore
            {
                PlayerName = GetPlayerName(),
                Score = PerformGame()
            };
           _scoresRepository.Add(gameScore);

            var result = "Congratulations " + gameScore.PlayerName + "! You scored " + gameScore.Score + " points";
            var scoreGame = _scoresRepository.GetAll();
           
            GameFinished?.Invoke(this, new MyEventArgs(scoreGame,result));
        }


        public int PerformGame()
        {
            _game.Play();
            return _game.Score;
        }

        public string GetPlayerName()
        {
            Console.Write("Enter your nickname: ");
            return Console.ReadLine();
        }
    }
}