﻿namespace Bookmarks.BL.Model
{
    public class Bookmark
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Weblink { get; set; }
    }
}
