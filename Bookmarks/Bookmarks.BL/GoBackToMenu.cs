﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookmarks.BL
{
    public class GoBackToMenu
    {
        public void GoBack()
        {
            Console.WriteLine("Naciśnij dowolny klawisz aby powrócić do głownego menu....");
            Console.ReadKey();
            Console.Clear();
        }
    }
}
