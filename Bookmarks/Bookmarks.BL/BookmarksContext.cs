﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bookmarks.BL.Model;

namespace Bookmarks.BL
{
    public class BookmarksContext:DbContext
    {
        public BookmarksContext():base("name=BookmarksRepository")
        {

        }
        public DbSet<Bookmark> Bookmarks { get; set; }
    }
}
