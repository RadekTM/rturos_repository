﻿using System;
using ConsoleTools.Menu;
using Bookmarks.BL;

namespace Bookmarks
{
    class Program
    {
        
        static void Main(string[] args)
        {
            BookmarksRepository _bookmarks = new BookmarksRepository();
            var exitOption = new ExitOption();
            var consoleMenu = new ConsoleMenu(new IMenuOption[] {
                new GeneralOption("Dodanie nowej zakładki",_bookmarks.Add),
                new GeneralOption("Lista zapisanych zakładek",_bookmarks.List),
                new GeneralOption("Eksport zakładki do pliku json",_bookmarks.SaveJsonFile),
                new GeneralOption("Otwarcie zakładki w przeglądarce",_bookmarks.OpenLinkInBrowser),
                exitOption
            });

            do
            {
                consoleMenu.Show();
            } while (!exitOption.IsExitRequested);
            Console.ReadLine();
        }

        
    }
}
