﻿namespace CommonLibrary.Models
{
    public class Group
    {
        public int Id { get; set; }
        public string GroupName { get; set; }

        public override string ToString()
        {
            return $@"Nazwa grupy: {GroupName}, Group Id {Id}";
        }
    }
}