﻿namespace CommonLibrary.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string PostBody { get; set; }
        public int UserId { get; set; }
        public int GroupId { get; set; }

        public override string ToString()
        {
            return $@"Post nr{Id}: {PostBody}, Autor {UserId}, Grupa{GroupId}";
        }

    }
}