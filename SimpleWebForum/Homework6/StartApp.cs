﻿using System;
using System.Threading.Tasks;
using CommonLibrary.Models;
using Homework6.communication;
using Homework6.communication.ComunicationInterfaces;
using Homework6.utils;
using Homework6.utils.utilsInterfaces;

namespace Homework6
{
    

    public class StartApp
    {
        private readonly IBackToMenu goBack = new BackToMenu();
        private readonly ILoggingInterface _logging = new LoggingInterface();
        private readonly IApiComunicationUser apiComunicationUser = new ApiComunicationUser();
        private readonly IUserActionsUser userActionsUser = new UserActionsUser();
        private readonly IUserActionsGroups userActionsGroups = new UserActionsGroups();
        private readonly IUserActionsPost userActionsPost = new UserActionsPost();

        public async Task Start()
        {
            bool userStatus = true;
            do
            {
                Console.WriteLine("Forum Codementors");
                Console.WriteLine("\na - Logowanie do forum");
                Console.WriteLine("b - utworzenie nowego konta");
                Console.WriteLine("x - Wyjście z programu");
                char menuitem = Console.ReadKey().KeyChar;
                Console.Clear();

                switch (menuitem)
                {
                    case 'a':
                        var currentUser = await apiComunicationUser.LogIn();
                            userStatus = false;
                            goBack.goBackToMenu();
                            await ManiMenu(currentUser);
                        break;
                    case 'b':
                        await userActionsUser.MenuCreateUser();
                        goBack.goBackToMenu();
                        userStatus = true;
                        break;

                    case 'x':
                        userStatus = false;
                        Console.ResetColor();
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Opcja niedostęna, proszę wybrać ponownie");
                        Console.Clear();
                        break;
                }
            } while (userStatus == true);
            Console.WriteLine("Zapraszamy ponownie");
            goBack.goBackToMenu();

        }

        private async Task ManiMenu(User user)
        {
            var choosenGroup = new Group();

            bool menu = true;
            do
            {
                Console.WriteLine("Forum Codementors");
                Console.WriteLine($"zalogowano jako: {user.Name}");
                Console.WriteLine($"Wybrana grupa to: {choosenGroup.GroupName}");
                Console.WriteLine("\na - Informacje o dostępnych grupach");
                Console.WriteLine("b - Wybór grupy");
                Console.WriteLine("c - opuszczenie aktualnej grupy");
                Console.WriteLine("d - dodanie nowej grupy i zalogowanie do niej");
                Console.WriteLine("e - dodaj post w aktualnej grupie");
                Console.WriteLine("f - wyświetl moje posty z aktualnej grupy");
                Console.WriteLine("g - wyświetl wszystkie posty z aktualnej grupy");
                Console.WriteLine("h - wyświetl wszystkie moje posty");

                Console.WriteLine("******************************");
                Console.WriteLine("1 - usunięcie konta");
                Console.WriteLine("2 - usunięcie grupy");

                Console.WriteLine("******************************");
                Console.WriteLine("x - Wyjście z programu");
                char menuitem = Console.ReadKey().KeyChar;
                Console.Clear();

                switch (menuitem)
                {
                    case 'a':
                        await userActionsGroups.MenuGetAllGroups();
                        goBack.goBackToMenu();
                        break;
                    case 'b':
                        await userActionsGroups.MenuGetAllGroups();
                        choosenGroup = await userActionsGroups.MenuJoinToGroup();
                        goBack.goBackToMenu();
                        break;
                    case 'c':
                        choosenGroup = userActionsGroups.MenuLeaveCurrentGroup(choosenGroup);
                        goBack.goBackToMenu();
                        break;
                    case 'd':
                        
                        choosenGroup = await userActionsGroups.MenuAddNewGroup();
                        goBack.goBackToMenu();
                        break;
                    case 'e':
                        await userActionsPost.MenuGetAllPostsFromGroup(choosenGroup.Id);
                        await userActionsPost.MenuNewPost(user.Id, choosenGroup.Id);
                        goBack.goBackToMenu();
                        break;
                    case 'f':
                        await userActionsPost.MenuMyPosts(user.Id, choosenGroup.Id);
                        goBack.goBackToMenu();
                        break;
                    case 'g':
                        await userActionsPost.MenuGetAllPostsFromGroup(choosenGroup.Id);
                        goBack.goBackToMenu();
                        break;
                    case 'h':
                        await userActionsPost.MenuGetAllMyPosts(user.Id);
                        goBack.goBackToMenu();
                        break;
                    case 'm':
                        await userActionsPost.MenuGetUserPost(user);
                        goBack.goBackToMenu();
                        break;
                    case '1':
                        menu = await userActionsUser.MenuDeleteuser(user);
                        goBack.goBackToMenu();
                        break;
                    case '2':
                        await userActionsGroups.MenuDeleteGroup(choosenGroup);
                        goBack.goBackToMenu();
                        break;
                    case 'x':
                        menu = false;
                        Console.ResetColor();
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Opcja niedostęna, proszę wybrać ponownie");
                        Console.Clear();
                        break;
                }
            } while (menu == true);
        }
    }
}
