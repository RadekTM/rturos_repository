﻿using System;
using System.Linq;
using System.Threading;
using Homework6.utils.utilsInterfaces;

namespace Homework6.utils
{
    public class LoggingInterface: ILoggingInterface
    {

        public bool WantCreateNewUser()
        {
            Console.WriteLine("\nCzy chcesz utworzyć nowe konto? Y/N");
            char choice;

            do
            {
                choice = Console.ReadKey().KeyChar;
                if (choice == 'Y' || choice == 'y')
                    return true;
                else if (choice == 'n' || choice == 'N')
                    return false;
                else
                    Console.WriteLine("podaj poprawn a litere: ");

            } while (!new[] { 'y', 'Y', 'n', 'N' }.Contains(choice));
            Console.WriteLine(" ");

            return false;
        }

        public bool WantDeleteUser()
        {
            Console.WriteLine("\nCzy chcesz kontynuować? Y/N");
            char choice;

            do
            {
                choice = Console.ReadKey().KeyChar;
                if (choice == 'Y' || choice == 'y')
                    return true;
                else if (choice == 'n' || choice == 'N')
                    return false;
                else
                    Console.WriteLine("Wybierz właściwą literę: ");

            } while (!new[] { 'y', 'Y', 'n', 'N' }.Contains(choice));

            Console.WriteLine(" ");
            return false;
        }

        public void ShowLoading()
        {
            Console.Clear();
            Console.WriteLine("\nPobieranie danych.");
            for (int i = 1; i < 100; i++)
            {
                Console.Write($"\nLoading");
                for (int j = 1; j < i; j++)
                {
                    Console.Write(".");

                    Thread.Sleep(100);
                }
            }
        }

    }
}
