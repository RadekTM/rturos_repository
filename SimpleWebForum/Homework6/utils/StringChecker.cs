﻿using System;
using Homework6.utils.utilsInterfaces;

namespace Homework6.utils
{
    public class StringChecker: IStringChecker
    {
        public string NullString(string InputText)
        {
            string tempString;
            do
            {
                Console.Write($"{InputText}");
                tempString = Console.ReadLine();
                if (string.IsNullOrEmpty(tempString))
                {
                    Console.Write($"{InputText}");
                    tempString = Convert.ToString(Console.ReadLine());
                }
            } while (string.IsNullOrEmpty(tempString) == true);
            return tempString;
        }
    }
}