﻿namespace Homework6.utils.utilsInterfaces
{
    public interface ILoggingInterface
    {
        bool WantCreateNewUser();
        bool WantDeleteUser();
        void ShowLoading();

    }
}