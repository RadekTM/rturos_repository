﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Homework6.utils.utilsInterfaces;

namespace Homework6.utils
{
    //source:https://docs.microsoft.com/pl-pl/dotnet/standard/base-types/how-to-verify-that-strings-are-in-valid-email-format

    public class MailChecker : IMailChecker
    {
        bool invalid = false;


        public string NullString(string InputText)
        {
            string tempString;
            do
            {
                Console.Write($"{InputText}");
                tempString = Console.ReadLine();
                if (string.IsNullOrEmpty(tempString))
                {
                    Console.Write($"{InputText}");
                    tempString = Convert.ToString(Console.ReadLine());
                }

                invalid = IsValidEmail(tempString);

            } while (invalid == false);
            return tempString;
        }

        private bool IsValidEmail(string strIn)
        {
            invalid = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
                    RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
                return false;

            try
            {
                return Regex.IsMatch(strIn,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        private string DomainMapper(Match match)
        {
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }
    }
}