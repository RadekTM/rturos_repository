﻿using System;
using Homework6.utils.utilsInterfaces;

namespace Homework6.utils
{
    public class BackToMenu:IBackToMenu
    {
        public void goBackToMenu()
        {
            Console.WriteLine(" ");
            Console.WriteLine("\nAby kontynuować naciśnij dowolny klawisz....");
            Console.ReadKey();
            Console.Clear();
        }
    }
}