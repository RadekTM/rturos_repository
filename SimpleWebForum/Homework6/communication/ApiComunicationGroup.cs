﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CommonLibrary.Models;
using Homework6.communication.ComunicationInterfaces;
using Newtonsoft.Json;

namespace Homework6.communication
{
    public class ApiComunicationGroup: IApiComunicationGroup
    {

        public async Task<Group> AddGroup(string groupName)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "group";
                var baseUri = new Uri(ConfigurationManager.AppSettings["forumEndpoint"]);
                var uri = new Uri(baseUri, adress);

                var group = new Group
                {
                    GroupName = groupName
                };

                var userSerialized = JsonConvert.SerializeObject(group);

                await httpClient.PostAsync(uri, new StringContent(userSerialized, Encoding.UTF8, "application/json"));

                return group;
            }
        }

        public async Task<IEnumerable<Group>> GetAllGroups()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "group";
                var baseUri = new Uri(ConfigurationManager.AppSettings["forumEndpoint"]);
                var uri = new Uri(baseUri, adress);

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var getResponce = await httpClient.GetAsync(uri);

                getResponce.EnsureSuccessStatusCode();

                var userString = await getResponce.Content.ReadAsStringAsync();

                var groups = JsonConvert.DeserializeObject<Group[]>(userString);

                return groups;
            }
        }

        public async Task<IEnumerable<Post>> GetGroupStatus(int groupid)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "post?groupid=" + groupid;
                var baseUri = new Uri(ConfigurationManager.AppSettings["forumEndpoint"]);
                var uri = new Uri(baseUri, adress);

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var getResponce = await httpClient.GetAsync(uri);

                getResponce.EnsureSuccessStatusCode();

                var userString = await getResponce.Content.ReadAsStringAsync();

                var user = JsonConvert.DeserializeObject<Post[]>(userString);

                return user;
            }
        }

        public async Task<Group> DeleteGroup(int groupid)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "/api/group?groupid=" + groupid;
                var baseUri = new Uri(ConfigurationManager.AppSettings["forumEndpoint"]);
                var uri = new Uri(baseUri, adress);

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var getResponce = await httpClient.DeleteAsync(uri);

                getResponce.EnsureSuccessStatusCode();

                var userString = await getResponce.Content.ReadAsStringAsync();

                var group = JsonConvert.DeserializeObject<Group>(userString);

                return group;
            }
        }

        public async Task<Group> GetGroupById(int id)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "group?id=" + id;
                var baseUri = new Uri(ConfigurationManager.AppSettings["forumEndpoint"]);
                var uri = new Uri(baseUri, adress);

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var getResponce = await httpClient.GetAsync(uri);

                getResponce.EnsureSuccessStatusCode();

                var userString = await getResponce.Content.ReadAsStringAsync();

                var group = JsonConvert.DeserializeObject<Group>(userString);

                return group;
            }
        }

        public async Task<Group> GetGroupByName(string name)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "group?name=" + name;
                var baseUri = new Uri(ConfigurationManager.AppSettings["forumEndpoint"]);
                var uri = new Uri(baseUri, adress);

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var getResponce = await httpClient.GetAsync(uri);

                getResponce.EnsureSuccessStatusCode();

                var userString = await getResponce.Content.ReadAsStringAsync();

                var group = JsonConvert.DeserializeObject<Group>(userString);

                return group;
            }
        }
    }
}