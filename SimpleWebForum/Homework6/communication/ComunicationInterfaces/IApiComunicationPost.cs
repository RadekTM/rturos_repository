﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CommonLibrary.Models;

namespace Homework6.communication.ComunicationInterfaces
{
    public interface IApiComunicationPost
    {
        Task AddPost(int userId, int groupId, string postLine);
        Task<IEnumerable<Post>> GetAllPost();
        Task<IEnumerable<Post>> GetPostByUserId(int id);
        Task<IEnumerable<Post>> GetPostByUserIdGroupId(int userid, int groupid);
        Task<IEnumerable<Post>> GetPostByGroupId(int id);

    }
}