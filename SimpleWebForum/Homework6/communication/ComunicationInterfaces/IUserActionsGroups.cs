﻿using System.Threading.Tasks;
using CommonLibrary.Models;

namespace Homework6.communication.ComunicationInterfaces
{
    public interface IUserActionsGroups
    {
        Task MenuGetAllGroups();
        Task<Group> MenuJoinToGroup();
        Group MenuLeaveCurrentGroup(Group group);
        Task<Group> MenuAddNewGroup();
        Task MenuDeleteGroup(Group group);
        Task<Group> DeleteGroup(Group group);

    }
}