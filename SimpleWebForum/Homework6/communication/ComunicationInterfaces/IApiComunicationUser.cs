﻿using System.Threading.Tasks;
using CommonLibrary.Models;

namespace Homework6.communication.ComunicationInterfaces
{
    public interface IApiComunicationUser
    {
        Task<User> LogIn();
        Task<User> GetUser(string inputedName, string imputedEmail);
        Task<User> AddUser(string name, string mail);
        Task<User> DeleteUser(int id);

    }
}