﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CommonLibrary.Models;

namespace Homework6.communication.ComunicationInterfaces
{
    public interface IApiComunicationGroup
    {
        Task<Group> AddGroup(string groupName);
        Task<IEnumerable<Group>> GetAllGroups();
        Task<IEnumerable<Post>> GetGroupStatus(int groupid);
        Task<Group> DeleteGroup(int groupid);
        Task<Group> GetGroupById(int id);
        Task<Group> GetGroupByName(string name);
    }
}