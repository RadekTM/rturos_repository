﻿using System.Threading.Tasks;
using CommonLibrary.Models;

namespace Homework6.communication.ComunicationInterfaces
{
    public interface IUserActionsPost
    {
        Task MenuGetUserPost(User user);
        Task MenuNewPost(int userId, int groupId);
        Task MenuMyPosts(int userId, int groupId);
        Task MenuGetAllMyPosts(int userId);
        Task MenuGetAllPostsFromGroup(int groupId);

    }
}