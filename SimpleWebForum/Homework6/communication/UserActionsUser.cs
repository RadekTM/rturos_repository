﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CommonLibrary.Models;
using Homework6.communication.ComunicationInterfaces;
using Homework6.utils;
using Homework6.utils.utilsInterfaces;

namespace Homework6.communication
{
    public class UserActionsUser : IUserActionsUser
    {
        private readonly IApiComunicationUser apiComunicationUser = new ApiComunicationUser();
        private readonly ILoggingInterface _logging = new LoggingInterface();
        private readonly IStringChecker stringChecker = new StringChecker();
        private readonly IMailChecker mailChecker = new MailChecker();

        public async Task<bool> MenuDeleteuser(User user)
        {
            Console.WriteLine($"Czy na pewno chcesz usunąć swoje konto");
            bool selection = _logging.WantDeleteUser();
            if (selection == true)
            {
                var loadingThread = new Thread(_logging.ShowLoading);
                loadingThread.Start();
                await apiComunicationUser.DeleteUser(user.Id);
                loadingThread.Abort();
                Console.WriteLine("Konto zostało usunięte");
                return false;
            }
            return true;
        }

        public async Task MenuCreateUser()
        {
            try
            {
                string name = stringChecker.NullString("Podaj nazwę użytkownika: ");
                string mail = mailChecker.NullString("Podaj e-mail: ");
                var loadingThread = new Thread(_logging.ShowLoading);
                loadingThread.Start();
                var result = await apiComunicationUser.AddUser(name, mail);
                loadingThread.Abort();
                if (result.Id == 0)
                {
                Console.WriteLine($"\nStoworzono użytkownika: {result.Name}, {result.Email}");
                }
                else if (result.Id != 0)
                {
                    Console.WriteLine("\nTaki użytkownik istnieje już w bazie!");
                }
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e);
            }
        }
    }
}