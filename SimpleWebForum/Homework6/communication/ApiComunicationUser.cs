﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CommonLibrary.Models;
using Homework6.communication.ComunicationInterfaces;
using Homework6.utils;
using Homework6.utils.utilsInterfaces;
using Newtonsoft.Json;

namespace Homework6.communication
{
    public class ApiComunicationUser : IApiComunicationUser
    {
        private readonly ILoggingInterface _logging = new LoggingInterface();
        private readonly IStringChecker stringChecker = new StringChecker();
        private readonly IMailChecker mailChecker = new MailChecker();

        public async Task<User> LogIn()
        {

            Console.WriteLine("Aby zalogować się do forum prosze o podanie imienia usera");
            string name = stringChecker.NullString("Podaj nazwe uzytkownika:");

            string email = mailChecker.NullString("Podaj adress e-mail:");
            var loadingThread = new Thread(_logging.ShowLoading);
            loadingThread.Start();
            var user = await GetUser(name, email);
            loadingThread.Abort();
            Console.Clear();
            if (user == null)
            {
                var choice = _logging.WantCreateNewUser();
                if (choice == true)
                {
                    var loadingThread1 = new Thread(_logging.ShowLoading);
                    loadingThread1.Start();
                    var newUser = await AddUser(name, email);
                    loadingThread1.Abort();
                    Console.WriteLine($"\nUtworzono konto i zalogowano jako: {newUser}");
                    user = newUser;
                }
                else
                {
                    user = null;
                }
            }
            else
            {
                Console.WriteLine($"\nZalogowano jako: {user}");
            }
            return user;
        }

        public async Task<User> GetUser(string inputedName, string imputedEmail)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "user?name=" + inputedName + "&email=" + imputedEmail;
                var baseUri = new Uri(ConfigurationManager.AppSettings["forumEndpoint"]);
                var uri = new Uri(baseUri, adress);

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var getResponce = await httpClient.GetAsync(uri);

                getResponce.EnsureSuccessStatusCode();

                var userString = await getResponce.Content.ReadAsStringAsync();

                var user = JsonConvert.DeserializeObject<User>(userString);

                return user;
            }
        }

        public async Task<User> AddUser(string name, string mail)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "user";
                var baseUri = new Uri(ConfigurationManager.AppSettings["forumEndpoint"]);
                var uri = new Uri(baseUri, adress);
                var user = new User
                {
                    Name = name,
                    Email = mail
                };

                var userSerialized = JsonConvert.SerializeObject(user);

                var getResponce = await httpClient.PostAsync(uri, new StringContent(userSerialized, Encoding.UTF8, "application/json"));
                var userString = await getResponce.Content.ReadAsStringAsync();

                var userq = JsonConvert.DeserializeObject<User>(userString);

                return userq;
            }

        }

        public async Task<User> DeleteUser(int id)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "/api/user?id=" + id;
                var baseUri = new Uri(ConfigurationManager.AppSettings["forumEndpoint"]);

                var uri = new Uri(baseUri, adress);

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var getResponce = await httpClient.DeleteAsync(uri);

                getResponce.EnsureSuccessStatusCode();

                var userString = await getResponce.Content.ReadAsStringAsync();

                var user = JsonConvert.DeserializeObject<User>(userString);

                return user;
            }
        }
    }
}