﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CommonLibrary.Models;
using Homework6.communication.ComunicationInterfaces;
using Newtonsoft.Json;

namespace Homework6.communication
{
    public class ApiComunicationPost : IApiComunicationPost
    {
        public async Task AddPost(int userId, int groupId, string postLine)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "post";
                var baseUri = new Uri(ConfigurationManager.AppSettings["forumEndpoint"]);
                var uri = new Uri(baseUri, adress);
                
                var group = new Post
                {
                    PostBody = postLine,
                    UserId = userId,
                    GroupId = groupId
                };

                var userSerialized = JsonConvert.SerializeObject(group);

                await httpClient.PostAsync(uri, new StringContent(userSerialized, Encoding.UTF8, "application/json"));
            }
        }

        public async Task<IEnumerable<Post>> GetAllPost()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "post";
                var baseUri = new Uri(ConfigurationManager.AppSettings["forumEndpoint"]);
                var uri = new Uri(baseUri, adress);

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var getResponce = await httpClient.GetAsync(uri);

                getResponce.EnsureSuccessStatusCode();

                var userString = await getResponce.Content.ReadAsStringAsync();

                var posts = JsonConvert.DeserializeObject<Post[]>(userString);

                return posts;
            }
        }

        public async Task<IEnumerable<Post>> GetPostByUserId(int id)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "post?userid=" + id;
                var baseUri = new Uri(ConfigurationManager.AppSettings["forumEndpoint"]);
                var uri = new Uri(baseUri, adress);

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var getResponce = await httpClient.GetAsync(uri);

                getResponce.EnsureSuccessStatusCode();

                var userString = await getResponce.Content.ReadAsStringAsync();

                var user = JsonConvert.DeserializeObject<Post[]>(userString);

                return user;
            }
        }

        public async Task<IEnumerable<Post>> GetPostByGroupId(int id)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "post?id=" + id;
                var baseUri = new Uri(ConfigurationManager.AppSettings["forumEndpoint"]);
                var uri = new Uri(baseUri, adress);

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var getResponce = await httpClient.GetAsync(uri);

                getResponce.EnsureSuccessStatusCode();

                var userString = await getResponce.Content.ReadAsStringAsync();

                var user = JsonConvert.DeserializeObject<Post[]>(userString);

                return user;
            }
        }


        public async Task<IEnumerable<Post>> GetPostByUserIdGroupId(int userid, int groupid)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var adress = "post/" + userid + "/" + groupid;
                var baseUri = new Uri(ConfigurationManager.AppSettings["forumEndpoint"]);
                var uri = new Uri(baseUri, adress);

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var getResponce = await httpClient.GetAsync(uri);

                getResponce.EnsureSuccessStatusCode();

                var userString = await getResponce.Content.ReadAsStringAsync();

                var user = JsonConvert.DeserializeObject<Post[]>(userString);

                return user;
            }
        }
    }
}