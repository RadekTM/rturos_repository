﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CommonLibrary.Models;
using Homework6.communication.ComunicationInterfaces;
using Homework6.utils;
using Homework6.utils.utilsInterfaces;

namespace Homework6.communication
{
    public class UserActionsGroups : IUserActionsGroups
    {
        private readonly IStringChecker stringChecker = new StringChecker();
        private readonly ILoggingInterface _logging = new LoggingInterface();
        private readonly IApiComunicationGroup apiComunicationGroup = new ApiComunicationGroup();
    
        public async Task MenuGetAllGroups()
        {
            try
            {
                var loadingThread = new Thread(_logging.ShowLoading);
                loadingThread.Start();
                var groups = await apiComunicationGroup.GetAllGroups();
                loadingThread.Abort();
                Console.Clear();
                foreach (var group in groups)
                {
                    Console.WriteLine(group);
                }
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task<Group> MenuJoinToGroup()
        {
            int chosenid = Convert.ToInt16(stringChecker.NullString("\nProszę podać id grupy do której chcesz dołączyc: "));
            var loadingThread = new Thread(_logging.ShowLoading);
            loadingThread.Start();
            Group chosenGroup = await apiComunicationGroup.GetGroupById(chosenid);
            loadingThread.Abort();
            Console.WriteLine(" ");
            Console.WriteLine($@"Dołączyłeś do grupy: {chosenGroup.GroupName}");
            return chosenGroup;
        }

        public Group MenuLeaveCurrentGroup(Group group)
        {
            Console.WriteLine($"opuszczenie grupy: {group.GroupName}");

            group.GroupName = String.Empty;
            group.Id = 0;
            return group;
        }

        public async Task<Group> MenuAddNewGroup()
        {
            string name = stringChecker.NullString("Podaj nazwę nowej grupy: ");
            var loadingThread = new Thread(_logging.ShowLoading);
            loadingThread.Start();
            await apiComunicationGroup.AddGroup(name);
            Group chosenGroup = await apiComunicationGroup.GetGroupByName(name);
            loadingThread.Abort();
            Console.WriteLine(" ");
            Console.WriteLine($@"Dołączyłeś do grupy: {chosenGroup.GroupName}");
            return chosenGroup;
        }

        public async Task MenuDeleteGroup(Group group)
        {
            Console.WriteLine($"Czy na pewno chcesz usunąć wybraną grupę");
            if (_logging.WantDeleteUser())
            {
                var loadingThread = new Thread(_logging.ShowLoading);
                loadingThread.Start();
                await DeleteGroup(group);
                loadingThread.Abort();
                Console.WriteLine(" ");
            }
        }

        public async Task<Group> DeleteGroup(Group group)
        {
            var groupToDelete = await apiComunicationGroup.GetGroupStatus(group.Id);
            if (groupToDelete == null)
            {
                Console.WriteLine($"Usuwam grupę: {group.GroupName}");
                var loadingThread = new Thread(_logging.ShowLoading);
                loadingThread.Start();
                var temp = await apiComunicationGroup.DeleteGroup(group.Id);
                loadingThread.Abort();
                Console.WriteLine($"Usunięto grupę: {temp.GroupName}");
                group.GroupName = String.Empty;
                group.Id = 0;
                return group;

            }
            Console.WriteLine("Grupa nie jest pusta, mie można jej usunąć");
            return group;

        }
    }
}