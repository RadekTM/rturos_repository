﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CommonLibrary.Models;
using Homework6.communication.ComunicationInterfaces;
using Homework6.utils;
using Homework6.utils.utilsInterfaces;

namespace Homework6.communication
{
    public class UserActionsPost: IUserActionsPost
    {
        private readonly IApiComunicationPost apiComunicationPost = new ApiComunicationPost();
        private readonly ILoggingInterface _logging = new LoggingInterface();
        private readonly IStringChecker stringChecker = new StringChecker();

        public async Task MenuGetUserPost(User user)
        {
            try
            {
                var loadingThread = new Thread(_logging.ShowLoading);
                loadingThread.Start();
                var temp = await apiComunicationPost.GetPostByUserId(user.Id);
                loadingThread.Abort();
                Console.WriteLine(" ");
                foreach (var post in temp)
                {
                    Console.WriteLine($"{post.PostBody}");
                }
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task MenuNewPost(int userId, int groupId)
        {
            string postLine = stringChecker.NullString("\nPodaj treść swojej wiadomości:\n");
            try
            {
                var loadingThread = new Thread(_logging.ShowLoading);
                loadingThread.Start();
                await apiComunicationPost.AddPost(userId, groupId, postLine);
                loadingThread.Abort();
                Console.WriteLine("\n ");
                Console.WriteLine("\nTwój post został dodany");

            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e);
            }

        }

        public async Task MenuMyPosts(int userId, int groupId)
        {
            try
            {
                var loadingThread = new Thread(_logging.ShowLoading);
                loadingThread.Start();
                var result = await apiComunicationPost.GetPostByUserIdGroupId(userId, groupId);
                loadingThread.Abort();
                Console.Clear();
                Console.WriteLine("Poniżej znajdują Twoje posty z wybranej grupy");
                int amount = result.Count();
                if (amount != 0)
                {
                    
                    foreach (var post in result)
                    {
                        Console.WriteLine($"Post: {post.PostBody} , post id: {post.Id}");
                    }
                }
                else
                {
                    Console.WriteLine("Brak wiadomości w tej grupie");
                }

            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task MenuGetAllMyPosts(int userId)
        {
            try
            {
                var loadingThread = new Thread(_logging.ShowLoading);
                loadingThread.Start();
                var posts = await apiComunicationPost.GetPostByUserId(userId);
                loadingThread.Abort();
                Console.Clear();
                Console.WriteLine("Poniżej znajdują się Twoje wszytskie posty: \n");
                int amount = posts.Count();
                if (amount != 0)
                {
                    foreach (var post in posts)
                    {
                        Console.WriteLine($"Post: {post.PostBody}     (id grupy w której jest post {post.GroupId})");
                    }
                }
                else
                {
                    Console.WriteLine("Brak wiadomości....");
                }

            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task MenuGetAllPostsFromGroup(int groupId)
        {
            try
            {
                var loadingThread = new Thread(_logging.ShowLoading);
                loadingThread.Start();
                var posts = await apiComunicationPost.GetPostByGroupId(groupId);
                loadingThread.Abort();
                Console.Clear();
                Console.WriteLine("Poniżej znajdują się wszytskie posty z wybranej grupy: \n");
                int amount = posts.Count();
                if (amount != 0)
                {
                    foreach (var post in posts)
                    {
                        Console.WriteLine($"Post: {post.PostBody}, autor id: {post.UserId}");
                    }
                }
                else
                {
                    Console.WriteLine("Brak wiadomości....");
                }

            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e);
            }
        }
    }
}