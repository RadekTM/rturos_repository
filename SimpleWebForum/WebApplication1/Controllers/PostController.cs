﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using CommonLibrary.Models;
using WebApplication1.Repository;

namespace WebApplication1.Controllers
{
    public class PostController:ApiController
    {
        public IEnumerable<Post> GetPosts()
        {
            using (var context = new Context())
            {
                return context.Posts.ToList();
            }
        }

        [HttpPost]
        public async Task<Post> AddNewPost(Post post)
        {
            using (var context = new Context())
            {
                context.Posts.Add(post);
                await context.SaveChangesAsync();
            }

            return post;
        }

        public async Task<List<Post>> GetByUserId(int userid)
        {
            using (var context = new Context())
            {
                var student1 = context.Posts.Where(c => c.UserId == userid).ToList();

                return student1;
            }
        }

        public async Task<List<Post>> GetByGroupId(int id)
        {
            using (var context = new Context())
            {
                var student1 = context.Posts.Where(c => c.GroupId == id).ToList();

                return student1;
            }
        }

        public async Task<List<Post>> GetGroupId(int groupid)
        {
            using (var context = new Context())
            {
                var student1 = context.Posts
                    .Where(c => c.GroupId == groupid).ToList().Capacity;

                if (student1 != 0)
                {
                    var student2 = context.Posts
                        .Where(c => c.GroupId == groupid).ToList();
                    return student2;
                }
                else
                {
                    return null;
                }
                
            }
        }

        public async Task<List<Post>> GetPost(int userid, int groupid)
        {
            using (var context = new Context())
            {

                var posts = context.Posts.Where(a => a.GroupId == groupid && a.UserId == userid).ToList();
                return posts; 
            }
        }



    }
}