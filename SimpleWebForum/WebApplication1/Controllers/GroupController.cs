﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using CommonLibrary.Models;
using WebApplication1.Repository;

namespace WebApplication1.Controllers
{
    public class GroupController : ApiController
    {

        public IEnumerable<Group> GetGroup()
        {
            using (var context = new Context())
            {
                return context.Groups.ToList();
            }
        }

        public async Task<Group> GetById(int id)
        {
            using (var context = new Context())
            {
                var group = context.Groups.FirstOrDefault(f => f.Id == id);
                return group;
            }

        }

        public async Task<Group> GetByName(string name)
        {
            using (var context = new Context())
            {
                var group = context.Groups.FirstOrDefault(f => f.GroupName == name);
                return group;
            }

        }

        [HttpPost]
        public async Task<Group> RegisterGroup(Group group)
        {
            using (var context = new Context())
            {
                context.Groups.Add(group);
                await context.SaveChangesAsync();
  
            }

            return group;
        }


        [HttpDelete]
        public async Task<Group> DeleteGroup(int groupid)
        {
            using (var context = new Context())
            {
                var tempUser = context.Groups.FirstOrDefault(f => f.Id == groupid);
                
                context.Groups.Remove(tempUser);
                await context.SaveChangesAsync();
                return tempUser;
            }
            
        }
    }
}