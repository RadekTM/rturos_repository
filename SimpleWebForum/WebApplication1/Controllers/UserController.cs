﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using CommonLibrary.Models;
using WebApplication1.Repository;

namespace WebApplication1.Controllers
{
    public class UserController:ApiController
    {
        public IEnumerable<User> Get()
        {
            using (var context = new Context())
            {
                return context.Users.ToList();
            }
        }

        [HttpPost]
        public async Task<User> RegisterUser(User user)
        {
            user.Id = 0;
            using (var context = new Context())
            {

                var userName = await context.Users.FirstOrDefaultAsync(f => f.Name == user.Name && f.Email == user.Email);
                if (userName == null)
                {
                    context.Users.Add(user);
                    await context.SaveChangesAsync(); 
                }
                else
                {
                    user = userName;
                }
            }

            return user;
        }

        [HttpDelete]
        public async Task<User> DeleteUser(int id)
        {
            using (var context = new Context())
            {
                var tempUser = context.Users.FirstOrDefault(f => f.Id == id);
                
                    context.Users.Remove(tempUser);
                    await context.SaveChangesAsync();
                return tempUser;

            }

            
        }


        public async Task<User> GetByName(string name)
        {
            using (var context = new Context())
            {
                var student = context.Users.FirstOrDefault(f => f.Name == name);
                return student;
            }

        }

        public async Task<User> GetAUser(string name, string email)
        {
            using (var context = new Context())
            {
                var student = context.Users.FirstOrDefault(p => p.Name == name && p.Email == email);
                return student;

            }

        }
    }
}