﻿using System.Data.Entity;
using CommonLibrary.Models;

namespace WebApplication1.Repository
{
    public class Context:DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Post> Posts { get; set; }
    }
}