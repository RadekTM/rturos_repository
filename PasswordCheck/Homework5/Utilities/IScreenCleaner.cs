﻿namespace Homework5.Utilities
{
    public interface IScreenCleaner
    {
        void ScreenClear();
        void WaitAndClean();
    }
}