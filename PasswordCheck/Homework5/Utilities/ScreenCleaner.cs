﻿using System;

namespace Homework5.Utilities
{
    public class ScreenCleaner : IScreenCleaner
    {
        public void ScreenClear()
        {
            Console.Clear();
        }

        public void WaitAndClean()
        {
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
            Console.Clear();
        }
    }
}