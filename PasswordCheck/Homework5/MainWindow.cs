﻿using System;
using Homework5.messages;
using Homework5.password;
using Homework5.Utilities;
using Menu;

namespace Homework5
{
    class MainWindow
    {
        static readonly IBasicMessage _basicMessage = new BasicMessage();
        readonly IScreenCleaner _screenCleaner = new ScreenCleaner();
        readonly IStringChecker _stringChecker = new StringChecker(_basicMessage);
        readonly IPasswordReceiver _passwordReceiver = new PasswordReceiver();


        public void userMenu()
        {
            var exitOption = new ExitOption();

            var consoleMenu = new ConsoleMenu(new IMenuOption[]
            {
                new GeneralOptions("Password checker", Run),
                exitOption
            });

            do
            {
                consoleMenu.ShowMenu();

            } while (!exitOption.IfExit);

            Console.ReadLine();
        }

        private void Run()
        {
            _screenCleaner.ScreenClear();
            _basicMessage.infoMessage();
            string tempPassword = _stringChecker.IsNullOrEmptyCheck(_passwordReceiver.ProvidetPassword());
            PasswordTests.Level1PasswordCheck(tempPassword);
            PasswordTests.Level2PasswordCheck(tempPassword);
            PasswordTests.Level3PasswordCheck(tempPassword);
            _screenCleaner.WaitAndClean();


        }
    }
}