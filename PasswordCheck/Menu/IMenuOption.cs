﻿namespace Menu
{
    public interface IMenuOption
    {
        string Description { get; }
        void Execute();
    }
}
