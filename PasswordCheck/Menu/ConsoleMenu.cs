﻿using System;
using System.Linq;

namespace Menu
{
    public class ConsoleMenu
    {
        private readonly IMenuOption[] _options;

        public ConsoleMenu(IMenuOption[] options)
        {
            _options = options.ToArray();
        }

        public void ShowMenu()
        {
            Console.WriteLine("Choose option:");
            int i = 1;

            foreach (var option in _options)
            {
                Console.WriteLine($"{i}. {option.Description}");
                i++;
            }

            uint choice;

            while (!uint.TryParse(Console.ReadLine(), out choice) || choice == 0 || choice > _options.Length)
            {
                Console.WriteLine("Wrong choice of option! Try again.");
            }
            _options[choice - 1].Execute();

        }
    }
}
