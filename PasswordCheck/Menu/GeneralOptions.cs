﻿using System;

namespace Menu
{
    public class GeneralOptions : IMenuOption
    {
        public string Description { get; }
        private Action _action;

        public void Execute()
        {
            _action();
        }

        public GeneralOptions(string description, Action action)
        {
            _action = action;
            Description = description;
        }
    }
}
