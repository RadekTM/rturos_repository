﻿namespace Menu
{
    public class ExitOption : IMenuOption
    {
        public string Description { get; } = "Exit";
        public bool IfExit { get; private set; }

        public void Execute()
        {
            IfExit = true;
        }
    }
}
