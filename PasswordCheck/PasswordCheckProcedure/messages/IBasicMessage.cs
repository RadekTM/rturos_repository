﻿namespace Homework5.messages
{
    public interface IBasicMessage
    {
        void CriteriaPassed(string input);
        void infoMessage();
        void StringErrorMessage();
        string CommonPasswordError();
        string DigitInsideError();
        string SpecialCharacterInsideError();
        string PasswordLenghtError(int _testedLenght);
        void CriteriaNotPassed(string input, string error);
    }
}