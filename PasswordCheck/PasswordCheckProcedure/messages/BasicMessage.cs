﻿using System;

namespace Homework5.messages
{
    public class BasicMessage : IBasicMessage
    {
       public void infoMessage()
        {
            Console.Write("Enter your password:");
        }

        public void StringErrorMessage()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Error - empty string.");
            Console.ResetColor();
            Console.Write($"Enter your password again:");
        }

        public void CriteriaPassed(string input)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Password meets all level{input} requirements");
            Console.ResetColor();
        }

        public void CriteriaNotPassed(string input, string error)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Password doesn`t meet - level{input} requirements");
            Console.WriteLine($"{error}");
            Console.ResetColor();
        }

        public string CommonPasswordError()
        {
            return "Password is very popular";
        }

        public string DigitInsideError()
        {
            return "Password should contain at least one digit inside";
        }

        public string SpecialCharacterInsideError()
        {
            return "Password should contain at least one special symbol inside";
        }

        public string PasswordLenghtError(int testedLenght)
        {
            return $"Password is to short. It should have at least {testedLenght} caracters";
        }
    }
}
