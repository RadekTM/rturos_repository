﻿using System;
using Homework5.messages;

namespace Homework5.password
{
    public class StringChecker : IStringChecker
    {
        private IBasicMessage _Message;

        public StringChecker(IBasicMessage message)
        {
            _Message = message;
        }

        public string IsNullOrEmptyCheck(string password)
        {

            do
            if (string.IsNullOrEmpty(password))
                {
                    _Message.StringErrorMessage();
                    password = Convert.ToString(Console.ReadLine());
                }
            while (string.IsNullOrEmpty(password) == true);
            return password;
        }
    }
}
