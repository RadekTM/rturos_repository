﻿using Homework5.Strategy;

namespace Homework5.AbstractFactory
{
    public class Level2Factory : AbstractFactory
    {
        public override TestPattern CreateTestPattern()
        {
            var builder = new Builder();
            builder.LengthCheckBuilder(8).OneDigitCheckerBuilder().CommonPasswordChecBuilder();
            return builder.ReadyChain();
        }
    }
}