﻿using Homework5.Strategy;

namespace Homework5.AbstractFactory
{
    public class Level3Factory : AbstractFactory
    {
        public override TestPattern CreateTestPattern()
        {
            var builder = new Builder();
            builder.LengthCheckBuilder(12).OneDigitCheckerBuilder().OneSpecialCharacterBuilder().CommonPasswordChecBuilder();
            return builder.ReadyChain();
        }
    }
}
