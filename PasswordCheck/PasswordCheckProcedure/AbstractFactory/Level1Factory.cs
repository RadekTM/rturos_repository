﻿using Homework5.Strategy;

namespace Homework5.AbstractFactory
{
    public class Level1Factory : AbstractFactory
    {
        public override TestPattern CreateTestPattern()
        {
            var builder = new Builder();
            builder.LengthCheckBuilder(5).CommonPasswordChecBuilder();
            return builder.ReadyChain();
        }
    }
}