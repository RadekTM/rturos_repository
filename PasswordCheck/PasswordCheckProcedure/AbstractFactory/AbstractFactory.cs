﻿using Homework5.Strategy;

namespace Homework5.AbstractFactory
{
    public abstract class AbstractFactory
    {
        public abstract TestPattern CreateTestPattern();
    }
}