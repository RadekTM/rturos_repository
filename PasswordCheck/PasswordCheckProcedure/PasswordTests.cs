﻿using Homework5.AbstractFactory;
using Homework5.messages;

namespace Homework5
{
    public class PasswordTests
    {
        static IBasicMessage message = new BasicMessage();

        public static void PaswordTests(string passwordToTest, AbstractFactory.AbstractFactory level, string levelNumber)
        {
            bool testResult = level.CreateTestPattern().VerifyTestSucess(passwordToTest, out string errorMessage);

            if (testResult)
            {
                message.CriteriaPassed(levelNumber);
            }
            else
            {
                message.CriteriaNotPassed(levelNumber, errorMessage);
            }
        }

        public static void Level1PasswordCheck(string passwordToCheck)
        {
            var level1 = new Level1Factory();
            PaswordTests(passwordToCheck, level1, "1");
        }

        public static void Level2PasswordCheck(string passwordToCheck)
        {
            var level2 = new Level2Factory();
            PaswordTests(passwordToCheck, level2, "2");
        }

        public static void Level3PasswordCheck(string passwordToCheck)
        {
            var level3 = new Level3Factory();
            PaswordTests(passwordToCheck, level3, "3");
        }

    }
}