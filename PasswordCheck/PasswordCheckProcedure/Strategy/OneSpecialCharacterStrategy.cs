﻿using System.Text.RegularExpressions;

namespace Homework5.Strategy
{
    public class OneSpecialCharacterStrategy:TestPattern
    {
        protected override string ErrorMessage()
        {
            return Message.SpecialCharacterInsideError();
        }

        protected override bool TestCheckIfPassed(string passwordToCheck)
        {
            return Regex.IsMatch(passwordToCheck, @"^.*([^\d\w\s]+|_).*$");
        }
    }
}