﻿
namespace Homework5.Strategy
{
    public class PasswordLenghtTestStrategy:TestPattern
    {
        private readonly int _testedLenght;

        public PasswordLenghtTestStrategy(int testedLenght)
        {
            _testedLenght = testedLenght;
        }

        protected override string ErrorMessage()
        {
            return Message.PasswordLenghtError(_testedLenght);
        }

        protected override bool TestCheckIfPassed(string passwordToCheck)
        {
            return passwordToCheck.Length >= _testedLenght;
            
        }
    }
}