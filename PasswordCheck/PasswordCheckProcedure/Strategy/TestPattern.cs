﻿using Homework5.messages;

namespace Homework5.Strategy
{
    public abstract class TestPattern
    {
        private TestPattern _nextTest;

        protected readonly IBasicMessage Message = new BasicMessage();

        protected abstract string ErrorMessage();

        protected abstract bool TestCheckIfPassed(string passwordToCheck);

        public bool VerifyTestSucess(string passwordToCheck, out string errorMessage)
        {
            if (TestCheckIfPassed(passwordToCheck))
            {
                if (_nextTest != null)
                {
                    return _nextTest.VerifyTestSucess(passwordToCheck, out errorMessage);
                }
                errorMessage = null;
                return true;
            }

            errorMessage = ErrorMessage();
            return false;
        }

        public TestPattern SetNextTest(TestPattern nextTest)
        {
            _nextTest = nextTest;
            return nextTest;
        }


    }
}