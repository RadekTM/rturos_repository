﻿namespace Homework5.Strategy
{
    public class Builder
    {
        private TestPattern _firstTestInChain;
        private TestPattern _lastTestInChain;

        private void AddNextToChain(TestPattern testInChain)
        {
            if (_firstTestInChain == null)
            {
                _firstTestInChain = testInChain;
            }

            if (_lastTestInChain == null)
            {
                _lastTestInChain = testInChain;
            }
            else
            {
                _lastTestInChain.SetNextTest(testInChain);
                _lastTestInChain = testInChain;
            }
        }

        public Builder LengthCheckBuilder(int length)
        {
            AddNextToChain(new PasswordLenghtTestStrategy(length));
            return this;
        }

        public Builder OneDigitCheckerBuilder()
        {
            AddNextToChain(new OneDigitTestStrategy());
            return this;
        }

        public Builder CommonPasswordChecBuilder()
        {
            AddNextToChain(new CommonPasswordTestStrategy());
            return this;
        }

        public Builder OneSpecialCharacterBuilder()
        {
            AddNextToChain(new OneSpecialCharacterStrategy());
            return this;
        }

        public TestPattern ReadyChain()
        {
            return _firstTestInChain;
        }
    }
}