﻿using System.Text.RegularExpressions;

namespace Homework5.Strategy
{
    public class OneDigitTestStrategy : TestPattern
    {
        protected override string ErrorMessage()
        {
            return Message.DigitInsideError();
        }

        protected override bool TestCheckIfPassed(string passwordToCheck)
        {
            return Regex.IsMatch(passwordToCheck, @"^.*[\d][\D].*$");
        }
    }
}