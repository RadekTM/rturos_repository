﻿using System;
using System.IO;
using System.Linq;

namespace Homework5.Strategy
{
    public class CommonPasswordTestStrategy : TestPattern
    {
        private readonly String[] _passwordFromList;

        public CommonPasswordTestStrategy()
        {
            _passwordFromList = File.ReadAllLines("probable-v2-top12000.txt");
        }

        protected override string ErrorMessage()
        {
            return Message.CommonPasswordError();
        }


        protected override bool TestCheckIfPassed(string passwordToCheck)
        {
            return !_passwordFromList.Contains(passwordToCheck);
        }
    }
}