﻿using System;

namespace ShopV2.BL.Model
{
    public class Product : IEquatable<Product>
    {
        public string Name { get;}
        public decimal Price { get;}

        public Product(string name, decimal price)
        {
            Name = name;
            Price = price;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (obj.GetType() != GetType())
                return false;
            return Equals((Product)obj);
        }

        public bool Equals(Product obj)
        {
            return obj.Name == this.Name;
        }
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}
