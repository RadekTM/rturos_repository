﻿using System.Collections.Generic;

namespace ShopV2.BL.Model
{
    public class Cart
    {

        Dictionary<Product, int> _cart = new Dictionary<Product, int>();

        public int Id { get; }
        public IEnumerable<KeyValuePair<Product, int>> Products =>_cart;
            

        public Cart(int id=0)
        {
            Id = id;
        }

        public void AddToCart(Product product, int amount)
        {
            if (_cart.TryGetValue(product, out int currentAmount))
            {
                _cart[product] = currentAmount + amount;
            }
            else
            {
                _cart[product] = amount;
            }
        }
        public decimal GetSum()
        {
            decimal sum = 0;
            foreach (var kvp in _cart)
            {
                sum += kvp.Key.Price * kvp.Value;
            }
            return sum;
        }
    }
}
