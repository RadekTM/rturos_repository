﻿using ShopV2.BL.Model;
using System.Collections.Generic;

namespace ShopV2.BL.Persistance
{
    public class CartDto : EntityBase
    {
        public List<CartPosition> Products { get; set; } = new List<CartPosition>();

        public CartDto()
        {

        }

        public CartDto(Cart cart)
        {
            Id = cart.Id;

            foreach (var item in cart.Products)
            {
                Products.Add(new CartPosition
                {
                    Amount = item.Value,
                    Product = new ProductDto(item.Key)
                });
            }
        }

        public Cart ToCart()
        {
            var cart = new Cart(Id);
            foreach (var item in Products)
            {
                cart.AddToCart(item.Product.ToProduct(), item.Amount);                
            }
            return cart;
        }
    }
}
