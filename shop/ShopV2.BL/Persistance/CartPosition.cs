﻿namespace ShopV2.BL.Persistance
{
    public class CartPosition : EntityBase
    {
        public int Amount { get; set; }
        public ProductDto Product { get; set; }


    }
}
