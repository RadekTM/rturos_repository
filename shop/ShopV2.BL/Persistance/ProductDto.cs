﻿using ShopV2.BL.Model;

namespace ShopV2.BL.Persistance
{
    public class ProductDto :EntityBase
    {
        public decimal Price { get; set; }
        public string Name { get; set; }
        
        public ProductDto(Product p)
        {
            Price = p.Price;
            Name = p.Name;
        }

        public ProductDto()
        {

        }

        public Product ToProduct()
        {
            return new Product(Name, Price);  
        }
    }
}