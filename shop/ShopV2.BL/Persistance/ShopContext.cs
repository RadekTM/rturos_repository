﻿using System.Data.Entity;

namespace ShopV2.BL.Persistance
{
    class ShopContext : DbContext 
    {
        public ShopContext():base("name=Shop")
        {

        }
        public DbSet<ProductDto> Products { get; set; }
        public DbSet<CartDto> Carts { get; set; }

    }
}
