﻿using ShopV2.BL.Model;
using ShopV2.BL.Persistance;

namespace ShopV2.BL
{
    public class CartRepository
    {
        public Cart CreateNew()
        {
            var dto = new CartDto();

            using (var ctx = new ShopContext())
            {
                ctx.Carts.Add(dto);
                ctx.SaveChanges();
            }

            return new Cart(dto.Id);
        }

        public void UpdateCart(Cart cart)
        {
            using (var ctx = new ShopContext())
            {
                CartDto entity = new CartDto(cart);
                ctx.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                foreach(var pos in entity.Products)
                {
                    ctx.Entry(pos).State = System.Data.Entity.EntityState.Modified;
                }
                ctx.SaveChanges();
            };
        }
    }
}
