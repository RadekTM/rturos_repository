﻿using ConsoleTools.Menu;
using ShopV2.BL;
using ShopV2.BL.Model;
using System;

namespace ShopV2
{
    class Program
    {
        static ProductRepository productRepository = new ProductRepository();
        static CartRepository cartRepository = new CartRepository();
        static Cart cart;

        static void Main(string[] args)
        {
            cart = cartRepository.CreateNew();

            var exitOption = new ExitOption();
            var consoleMenu = new ConsoleMenu(new IMenuOption[] {
                ///()=>{ } pusta lambda
                new GeneralOption("Add to shop",AddToShop),
                new GeneralOption("Add product to cart",AddToCart),
                exitOption
            });

            do
            {
                consoleMenu.Show();
            } while (!exitOption.IsExitRequested);
            Console.WriteLine($"Total: {cart.GetSum()}");
            Console.ReadLine();
        }

        private static void AddToCart()
        {
            Console.WriteLine("Product name:");
            var name = Console.ReadLine();
            Console.WriteLine("Product amount:");
            var amount = Convert.ToInt32(Console.ReadLine());

           var p = productRepository.GetProduct(name);
            if (p == null)
            {
                Console.WriteLine("Product not found");
                return;
            }
            cart.AddToCart(p, 1);
            cartRepository.UpdateCart(cart);
        }

        static void AddToShop()
        {
            Console.WriteLine("Product name:");
            var name = Console.ReadLine();
            Console.WriteLine("Product price:");
            var price = Convert.ToDecimal(Console.ReadLine());

            var product = new Product(name, price);
            productRepository.AddProduct(product);
        }
    }
}
